---
title: GreenOrDie.org
layout: default
---

<div class="small text-right mt-0 pt-0" markdown="1">

Each person on this planet produces [13](https://greenordie.org/q/world) kg CO<sub>2</sub> daily.<sup>*</sup>     
Most western countries do more than 20 kg/person/day.   
Long-term survival limit is 3,5 kg.

<div class="small mt-0 pt-0 ml-5 text-light">* in average, of course</div>

</div>

<div class="container p-0" id="mithrill"></div>

<script>

    m.mount(document.getElementById("mithrill"), {
        view: () => { return m(searchForm) }
    })

</script>

<div class='mt-5 pt-5 text-light text-center border-top' markdown="1"> 

<i class="fa-solid fa-earth-americas big" style="color: #FFFFFF;"></i>&nbsp;
[Learn more how you can help save the planet](/help)
&nbsp;<i class="fa-solid fa-earth-americas big" style="color: #FFFFFF;"></i>

</div>



