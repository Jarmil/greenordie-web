---
title: Help GreenOrDie.org
layout: default
---

## The mission: data


GreenOrDie.org focus on providing best possible carbon footprint data. 
This thing alone can reduce the average CO<sup>2</sup> emissions.
We believe that there is a lot of people who want to behave responsibly, track and reduce their footprint, little by little.

And GreenOrDie.org aims to be a trusted data source for such people. 

.

## How can you help?

GreenOrDie.org needs your help in three areas: 

- spreading the word (for everyone)
- making this website and related tools better (this is for IT gurus) 
- providing more and better data (for data persons)

.

## Spread the word

Are you a blogger, journalist? Give GreenOrDie.org a link. Every result has a permalink below, so you can easily
integrate them into your articles and posts: see, for example, how United States carbon dioxide emissions per capita 
are more than [100x higher](https://greenordie.org/q/usa_vs_chad) compared to typical Africa country.

Tweet about our results. Make a Facebook / Instagram / Reddit / Mastodon/ YouFavouriteSocialNetwork post about GreenOrDie.
Talk about carbon dioxide. Talk about GreenOrDie. Data are fun, make use of it ;-) 

Be creative and do it now. We didn't have too much time for change.   

.

## Making this website better 

If you know what ```git``` means, check out our repositories and feel free to improve the project. There is a lot of work to be done: 

[Website - frontend](/repos/web)  
[Data and python tools - backend](/repos/data)  

.

## Providing more and better data

Do you know some carbon footprint data for things or activities we do not list yet? 
All our data are in ```.yaml``` files in the data project, so you can easily extend this database: 

[Data project](/repos/data)  


