---
title: Web project git repository
layout: default
---

This web should be nicer. It needs a nice UX, responsivity, sharing buttons, et cetera.      
Can you fix it? [This way, please.](https://gitlab.com/jarmil/greenordie-web.git)


<div class='small mt-5 text-light' markdown="1"> 

P.S.: Pythoner? Data scientist? [Check out the data project](/repos/data)

</div>



