---
title: Data project git repository
layout: default
---

The core of GreenOrDie is data. Good data. 

Next milestone is to collect CO<sub>2</sub> footprint data for 1000 most used products.

The target is to have such data for 10.000 most used products/activities. This will cover
more than 90% of average person's carbon footprint, providing exact data for emission reduction.  

Carbon footprint data are compiled from various sources, listed in .yaml files,
then searched and cross-verified via python scripts.

Sounds interesting? Are you a pythoner or a data person? 
[This way, please.](https://gitlab.com/jarmil/greenordie-data.git)







